import React from 'react'
import HeroList from '../heros/HeroList';

export const MarvelScreen = _ => {
    return (
        <>
            <h2>Marvel screen </h2>
            <HeroList publisher={'Marvel Comics'} />
        </>
    )
}

export default MarvelScreen;
