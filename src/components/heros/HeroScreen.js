import React from 'react'
import { useParams, Redirect } from 'react-router-dom';
import { getHeroById } from '../../selectors/getHerosById';

export const HeroScreen = ({ history }) => {
    const { heroId } = useParams()
    const hero = getHeroById(heroId)

    if (!hero) {
        return <Redirect to="/" />;
    }
    const {
        superhero,
        publisher,
        alter_ego,
        first_appearance,
        characters
    } = hero;

    const handleReturn = () => {

        if (history.length <= 2) {
            history.push('/');
        } else {

            history.goBack();
        }
    }


    return (
        <div className="row mt-5">
            <div className="col-4" >
                <img src={`../assets/heroes/${heroId}.jpg`}
                    className="img-thumbnail"
                    alt={superhero}
                />
                <div className="col-8">
                    <h3>{superhero}</h3>
                    <ul className="list-group list-group-flush">
                        <li className="list-group-item">Alter ego: {alter_ego}</li>
                        <li className="list-group-item">Publisher: {publisher}</li>
                        <li className="list-group-item">First apperance: {first_appearance}</li>
                    </ul>
                </div>
            </div>
            <button
                className="btn btn-primary"
                onClick={handleReturn}
            >Regresar</button>
        </div>
    )
}

export default HeroScreen;
