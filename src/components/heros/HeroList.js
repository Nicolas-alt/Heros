import React, { useMemo } from 'react'
import { getHeroByPublisher } from '../../selectors/getHerosByPublisher';
import { HeroCard } from './HeroCard';

export const HeroList = ({ publisher }) => {
    const heroes = useMemo(() => { getHeroByPublisher(publisher) }, [publisher])

    return (
        <div className="d-flex flex-wrap">
            {
                heroes.map(hero => (
                    <HeroCard key={hero.id} {...hero} />
                ))
            }
        </div>
    )
}

export default HeroList;
