import React from "react";
import {Switch, Route, Redirect} from "react-router-dom";

import MarvelScreen from "../components/marvel/MarvelScreen";
import {Navbar} from "../components/ui/nav/Nav";
import HeroScreen from "../components/heros/HeroScreen";
import DcScreen from "../components/dc/DcScreen";

export const DashBoardRoutes = _ => {
  return (
    <>
      <Navbar />
      <Switch>
        <Route exact path="/marvel" component={MarvelScreen} />
        <Route path="/hero/:heroId" component={HeroScreen} />
        <Route path="/dc" component={DcScreen} />
        <Redirect to="/marvel" />
      </Switch>
    </>
  );
}

export default DashBoardRoutes; 
