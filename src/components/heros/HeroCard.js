import React from 'react'
import {Link} from 'react-router-dom'

export const HeroCard = ({
  id,
  superhero,
  publisher,
  alter_ego,
  first_appearance,
  characters
}) => {
  return (
    <div className="card ms-3" style={{"maxWidth": '18rem'}}>
      <img src={`./assets/heroes/${id}.jpg`} className="card-img-top" alt="..." />
      <div className="card-body">
        <h5 className="card-title">{superhero}</h5>
        <p className="card-text">{alter_ego}</p>
        <p className="card-text">{first_appearance}</p>
      </div>
      <Link to={`./hero/${id}`}> Ver maś  </Link>
    </div>
  )
}

