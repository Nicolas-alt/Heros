import React from 'react';

export const SearchScreen = _ => {
  return (
    <>
      <h2>Search Screen</h2>
      <div className="row" >
        <div className="col-5">
          <h5>Search</h5>
          <form >
            <input type="text"
              placeholder="Search.." />
          </form>
        </div>
        <div className="col-7">
        </div>
      </div>
    </>
  )
}

export default SearchScreen;
