import React from 'react'
import HeroList from '../heros/HeroList';

export const DcScreen = () => {

    return (
        <>
            <h2>Marvel screen </h2>
            <HeroList publisher={'DC Comics'} />
        </>
    )
}

export default DcScreen;
